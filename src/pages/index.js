import Layout from "@/components/Layout";
import { Typography } from "@mui/material";
import { useAppContext } from "@/lib/appContext";
import TreeRenderer from "@/components/TreeRenderer";
import { INITIAL_STATE } from "@/config/INITIAL_STATE";



export default function Home() {

  const [ctx, setCtx] = useAppContext()
  return (
   <Layout 
      title = {
        <Typography 
          variant="h4" 
          fontWeight={800}
          sx = {{
              fontSize: {xs: "1em"},
              textAlign: {xs: "center", sm: "center"}
          }}
        >
          STANDARD OPERATING PROCEDURE RECEPTION DTI
      </Typography>
    }>

     <TreeRenderer data = {INITIAL_STATE}/>
   
   </Layout>
  );
}
