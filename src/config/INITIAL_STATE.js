
export const INITIAL_STATE = { 
    label: "Persona con problema alla tessera (Ricezione DTI)",
    messages: ["Chiedere se la persona è un collaboratore o uno studente?"],
    option: null,
    options: [
      {
        value: "colab",
        label: "Collaboratore",
        messages: ["Andare all ufficio delle risorse umane 2° piano (Matteo Lunini)"]
      },
      {
        value: "student",
        label: "Studente",
        messages: ["Tipo di studente"],
        option: null,
        options: [
          {
            value: "DFA",
            label: "Studente DFA,DEASS o DACD",
            messages: ["Dire allo studente di rivolgersi al suo segretariato di riferimento"]
          },
          {
            value: "FC",
            label: "Studente FC",
            messages: ["Consegnare le tessere FC alla responsabile amministrativa (Sabrina Zanfrini)"]
          },
          {
            value: "DTI",
            label: "Studente DTI o DACD CE",
            messages: ["Controllare se la tessera è scaduta"],
            option: null,
            options: [
              {
                key: "SI",
                value: "SCADUTA",
                label: "Si, è scaduta",
                messages: [
                  "Lo stagista deve ritirare la tessera studente in Ricezione DTI",
                  "Indirizzare lo studente al portale studenti per segnalare il problema e richiedere una nuova tessera Assistenza invia una richiesta"
                ]
              },
              {
                key: "NO",
                value: "NON-SCADUTA",
                label: "No, non è scaduta",
                messages: ["Qual è il problema?"],
                option: null,
                options: [
                  {
                    value: "dannegiata",
                    label: "Tessera dannegiata",
                    messages: [
                      "Lo stagista deve ritirare la tessera studente in Ricezione DTI",
                      "Indirizzare lo studente al portale studenti per segnalare il problema e richiedere una nuova tessera Assistenza invia una richiesta"
                    ]
                  },
                  {
                    value: "smarrita",
                    label: "Tessera smarrita",
                    messages: [
                      "Indirizzare lo studente al portale studenti per segnalare il problema e richiedere una nuova tessera Assistenza invia una richiesta"
                    ]
                  },
                  {
                    value: "rubata",
                    label: "Tessera rubata",
                    messages: [
                      "Indirizzare lo studente al portale studenti per segnalare il problema e richiedere una nuova tessera Assistenza invia una richiesta"
                    ]
                  },
                  {
                    value: "mai-ricevuta",
                    label: "Tessera mai ricevuta",
                    messages: ["Controlla il porta tessere"],
                    option: null,
                    options: [
                      {
                        key: "presente",
                        value: true,
                        label: "Se la tessera è presente",
                        messages: ["Se tessera presente, consegnar la tessera allo studente"]
                      },
                      {
                        key: "non-presente",
                        value: false,
                        label: "Se la tessera non è presente",
                        messages: ["Dirgli di aspettare la e mail della Segreteria FB"]
                      },

                    ]
                  },
                  {
                    value: "non-funzionante",
                    label: "Tessera non funzionante (smagnetizzata o problema non identificato)",
                    messages: ["Verificare che effettivamente sia smagnetizzata (check porta esterna Lettore Nero Blocco B)"],
                    option: null,
                    options: [
                      {
                        value: "verde",
                        label: "Esce verde",
                        messages: ["Studente Mobilità?"],
                        option: null,
                        options: [
                          {
                            value: "si",
                            label: "Si",
                            messages: ["Compilare il matchform tessere malfunzionanti con lo studente, inoltrare le informazioni a collaboratrice dell \’ ufficio international\"(Elisabetta Caneva)"]
                          },
                          {
                            value: "no",
                            label: "No",
                            messages: ["Compilare il matchform tessere malfunzionanti con lo studente, inoltrare le informazioni a collaboratrice FB(Sabrina Streit)"]
                          }
                        ]
                      },
                      {
                        value: "rosso",
                        label: "Esce rosso",
                        messages: [
                          "Compilare il matchform tessere malfunzionanti con lo studente e consegnare la tessera a logistica Campus est",
                          "Una volta riconsegnata la tessera studente da parte della logistica Campus Est, avvisare lo studente via e mail che può venire a ritirare la tessera."
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }