import { Alert, Grid, Typography } from '@mui/material'
import React from 'react'
import { FaQuestionCircle } from 'react-icons/fa'

export default function Messages({data, icon}) {
        return data && Array.isArray(data) && data.map((message, index) => (
            <Grid item sm={12} key={index}>
                <Alert severity='info' icon={icon ? <FaQuestionCircle size="1.2em" color="darkred" /> : false} sx={{ alignItems: "center" }}>
                    <Typography variant="body1" color="secondary" fontWeight={600}>
                        {message}
                    </Typography>
                </Alert>
            </Grid>
        ))
}

