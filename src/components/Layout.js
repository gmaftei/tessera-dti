
import { Box, Container, Grid, IconButton, Toolbar } from '@mui/material'
import React from 'react'
import { MdRefresh } from 'react-icons/md'

export default function Layout({title, children}) {
  return (
    <Container maxWidth="md" 
        sx = {{height:"100vh", p: 2}}
    >
        <Box sx = {theme => ({
            border: `${theme.spacing(0.1)} solid ${theme.palette.primary.dark} ` ,
            height: "100%",
            overflow: "hidden"

     })}>
        <Toolbar
            sx = {
                t => ({
                    background: t.palette.primary.light,
                    color: t.palette.primary.contrastText
                })
            }
        >
            <Grid container>
                <Grid item xs = {true}>
            {title}
            </Grid>
            <IconButton size="small" color="inherit" onClick={()=> location.reload()}>
                <MdRefresh/>
            </IconButton>
            </Grid>
        </Toolbar>
        <Box sx = {t=>({
            px:2, 
            background:t.palette.grey[200],
            height:"88.5vh", overflow:"auto", p:2
        })}>
        {children}
            </Box>      
        </Box>
    </Container>
  )
}
