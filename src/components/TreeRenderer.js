import { Alert, Button, Grid, Typography } from '@mui/material'
import React, { Fragment, useState } from 'react'
import { FaQuestionCircle } from 'react-icons/fa';
import Messages from './Messages';


const TreeRenderer = ({ data }) => {
    const [selectedOptions, setSelectedOptions] = useState({});

    const handleClick = (option, label) => {
        setSelectedOptions({
            ...selectedOptions,
            [label]: option.value
        });
    };

    const getActiveOption = (label) => {
        return selectedOptions[label];
    };

    const renderOptions = (options, label) => {
        return (
            <Grid item sm={12}>
                
                <Grid container spacing={2} justifyContent="center">
                    {options.map((option, index) => (
                        <Fragment key={index}>
                            {option.value && (
                                <Grid item sm={12} xs={12} key={index}>
                                    <Button
                                        fullWidth={true}
                                        variant={getActiveOption(label) === option.value ? "contained" : "outlined"}
                                        onClick={() => handleClick(option, label)}
                                    >
                                        {option.label}
                                    </Button>
                                    {(getActiveOption(label) === option.value)   && <Messages data = {option.messages} /> }
                                    {(getActiveOption(label) === option.value) && option.options && renderOptions(option.options,  option.label)}
                                    
                                </Grid>
                            )}
                        </Fragment>
                    ))}
                </Grid>
            </Grid>
        );
    };

    return (
        <Grid container key={data.label} justifyContent="center" spacing={2}>

            <Grid item sm = {12} xs = {12}>
                <Typography variant="body1" fontWeight={600} textAlign="center">
                    {data.label}
                </Typography>
            </Grid>

            <Messages data = {data.messages} icon = {true} />
            
            {data.options && renderOptions(data.options, data.label)}
           
            
          
        </Grid>
    );
};

export default TreeRenderer


