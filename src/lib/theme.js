import { Roboto, Urbanist, Montserrat } from 'next/font/google';
import { createTheme } from "@mui/material/styles"
import { blue, green, orange, red } from '@mui/material/colors';

const roboto = Roboto({
  weight: ['300', '400', '500', '700'],
  subsets: ['latin'],
  display: 'swap',
});

const urbanist = Urbanist({
  subsets:["latin-ext"],
  weight: ['300', '400', '500', '700'],
})

const mont = Montserrat({
  subsets:["latin-ext"],
  weight: ['300', '400', '500', '700'],
})
// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: blue[900]
    },
    secondary: {
      main: red[900]
    },
    error: {
      main: red.A400,
    },
    warning: {
      main: orange[800]
    },
    info: {
      main: blue[300]
    },
    success: {
      main: green[300]
    },
    blue: {
      main: blue.A700
    }
  },
  typography: {
    fontFamily: roboto.style.fontFamily,
  }
});

export default theme;
